﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CongCuLab2
{
    public partial class Form1 : Form
    {

        //Khai bao myContainer de luu giu thong tin database
        Model1Container myContainer;
        public Form1()
        {
            InitializeComponent();
            myContainer = new Model1Container();
        }
        private void btnCount_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Co " + myContainer.Customers1.Count().ToString() + " nguoi.");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Obtain the first record.
            Customer ThisCustomer = null;
            if (myContainer.Customers1.Count() > 0)
                ThisCustomer = myContainer.Customers1.First();
            else
            {
                // Display an error message if there are no records to delete.
                MessageBox.Show("No Records to Delete");
                return;
            }
            // Delete it.
            myContainer.Customers1.DeleteObject(ThisCustomer);
            myContainer.SaveChanges();
            // Inform the user.
            MessageBox.Show("Deleted " + ThisCustomer.CustomerID.ToString());
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Customer ThisCustomer = myContainer.Customers1.CreateObject();
            Random ThisValue = new Random(DateTime.Now.Millisecond);
            ThisCustomer.Firstname = ThisValue.Next().ToString();
            ThisCustomer.LastName = ThisValue.Next().ToString();
            ThisCustomer.Address = ThisValue.Next().ToString();
            cuContainer.Customers1.AddObject(ThisCustomer);
            cuContainer.SaveChanges();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            cuContainer.SaveChanges();
            Close();
        }
    }
}
